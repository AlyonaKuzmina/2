﻿using System;

namespace лаб2_номер_2
{
    class Program
    {
        static void Main(string[] args)
        {
            // f(x) = 1/17 + ln(|x|/3) + x/7 + max(x, 1) + 2tg(x/3) + x^(2*x)

            //Выводим информацию на консоль
            Console.WriteLine("Введите x:");

            //Ждем от пользователя ввода строки
            string xstr = Console.ReadLine();

            //Конвертируем строковый тип в тип double
            double x = Convert.ToDouble(xstr);

            //Объявляем y
            double y;
            y = 1 / 17.0 + Math.Log(Math.Abs(x) / 3) + x / 7 + Math.Max(x, 1) + 2 * Math.Tan(x / 3) + Math.Pow(x, (2 * x));

            // Выводим информацию о значение y.
            Console.WriteLine(y);
        }
    }
}
