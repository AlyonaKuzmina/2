﻿using System;

namespace лаб2_номер_3
{
    class Program
    {
        static void Main(string[] args)
        {
            //f(x, y, z) = 1/17 + ln(|y|/3) + z/7 + max(x, 1)

            // Выводим информацию на консоль.
            Console.WriteLine("Введите x:");
            // Ждем от пользователя ввода строки для x.
            string xstr = Console.ReadLine();

            Console.WriteLine("Введите y:");
            // Ждем от пользователя ввода строки для y.
            string ystr = Console.ReadLine();

            Console.WriteLine("Введите z:");
            // Ждем от пользователя ввода строки для z.
            string zstr = Console.ReadLine();

            // Конвертируем строковой тип в тип double.
            double x = Convert.ToDouble(xstr);
            // Конвертируем строковой тип в тип double.
            double y = Convert.ToDouble(ystr);
            // Конвертируем строковой тип в тип double.
            double z = Convert.ToDouble(zstr);

            // Объявляем f.
            double f;
            f = 1 / 17.0 + Math.Log(Math.Abs(y) / 3) + z / 7 + Math.Max(x, 1);

            // Выводим информацию о значение f.
            Console.WriteLine(f);
        }
    }
}
