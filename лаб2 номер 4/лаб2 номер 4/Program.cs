﻿using System;

namespace лаб2_номер_4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Учебник
            Console.WriteLine("Цифровая модель учебника");

            //Заданные атрибуты цифровой модели
            //Количество страниц
            int PagesCount;
            //Высота учебника(см)
            double Height;
            //Ширина учебника(см)
            double Width;
            //Длина учебника(см)
            double Lenght;

            //Вычисляем атрибуты цифровой модели
            // Площадь учебника
            double Square;
            // Объем учебника
            double Volume;
            //Количество краски для печати
            double PaintCount;
            //Количество ниток для сшивания страниц
            double ThreadCount;

            // Ввод информации в программу.
            Console.WriteLine("Введите количество страниц:");
            string PagesCountStr = Console.ReadLine();
            PagesCount = Convert.ToInt32(PagesCountStr);
  
            Console.WriteLine("Введите высоту:");
            string HeightStr = Console.ReadLine();
            Height = Convert.ToDouble(HeightStr);
            
            Console.WriteLine("Введите ширину:");
            string WidthStr = Console.ReadLine();
            Width = Convert.ToDouble(WidthStr);

            Console.WriteLine("Введите длину:");
            string LenghtStr = Console.ReadLine();
            Lenght = Convert.ToDouble(LenghtStr);

            // Расчет вычисляемых атрибутов.
            Square = Lenght * Width;
            Volume = Square * Height;
            PaintCount = 1 / 2 * PagesCount + Volume;
            ThreadCount = 2 * Height + Width + PagesCount;


            // Вывод информации на консоль.
            Console.WriteLine("Количество страниц: " + PagesCount);
            Console.WriteLine("Высота: " + Height + " см.");
            Console.WriteLine("Ширина: " + Width + "см.");
            Console.WriteLine("Длина: " + Lenght + "см.");
            Console.WriteLine("Площадь: " + Square + " см^2.");
            Console.WriteLine("Объем: " + Volume + " см^3.");
            Console.WriteLine("Количество краски:" + PaintCount + "мл.");
            Console.WriteLine("количество ниток:" + ThreadCount + "см");
        }
    }
}
