﻿using System;

namespace лаб2_номер1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Дан объем шара, найти площадь поверхности, половину радиуса и четверть объема
            Console.WriteLine("Введите объем шара");
            double V = Convert.ToDouble(Console.ReadLine());

            //Радиус
            double R = Math.Cbrt(3 * V / Math.PI / 4);
            Console.WriteLine("радиус:" + R);

            //Площадь поверхности
            double S = 4 * Math.PI * R * R;
            Console.WriteLine("площадь поверхности: " + S);

            //Половина радиуса
            double r = R / 2;
            Console.WriteLine("половина радиуса: " + R / 2);

            //Четверть объема
            double v = V / 4;
            Console.WriteLine("четверть объема: " + V / 4);
        }
    }
}
